package com.pasotti.matteo.apptictrac.dagger.module;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;

import com.pasotti.matteo.apptictrac.mainscreen.MainContract;
import com.pasotti.matteo.apptictrac.mainscreen.UsersAdapter;
import com.pasotti.matteo.apptictrac.util.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActModule {

    private final MainContract.View view;

    public MainActModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    MainContract.View provideView() { return view; }

    @Provides
    @ActivityScope
    UsersAdapter provideUsersAdapter(Activity activity) {
        return new UsersAdapter(activity);
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideLinearLayoutManager(Activity activity) {
        return new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
    }
}
