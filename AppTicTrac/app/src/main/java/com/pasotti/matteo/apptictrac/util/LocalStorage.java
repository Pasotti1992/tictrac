package com.pasotti.matteo.apptictrac.util;

import com.pasotti.matteo.apptictrac.pojo.User;

import java.util.List;

/**
 * Created by matte on 01/11/2017.
 */

public interface LocalStorage {

    void addUserToPreferences (User user);

    void removeUserFromPreferences (User user);

    List<User> getUsersFromPref();
}
