package com.pasotti.matteo.apptictrac.pojo;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("infos")
    private String infos;

    public User(){}

    public User(String name, String email, String infos) {
        this.name = name;
        this.email = email;
        this.infos = infos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }
}
