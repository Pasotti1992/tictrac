package com.pasotti.matteo.apptictrac.dagger.module;

import android.app.Activity;

import com.pasotti.matteo.apptictrac.util.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity providesActivity(){ return activity; }
}
