package com.pasotti.matteo.apptictrac.dagger.component;

import android.app.Application;

import com.pasotti.matteo.apptictrac.dagger.module.AppModule;
import com.pasotti.matteo.apptictrac.dagger.module.NetModule;
import com.pasotti.matteo.apptictrac.util.SharedPrefStorage;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    Retrofit retrofit();

    Application provideApplication();

    SharedPrefStorage provideSharedPrefStorage();
}
