package com.pasotti.matteo.apptictrac.mainscreen;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pasotti.matteo.apptictrac.R;
import com.pasotti.matteo.apptictrac.common.MyApplication;
import com.pasotti.matteo.apptictrac.dagger.component.DaggerMainActComponent;
import com.pasotti.matteo.apptictrac.dagger.module.ActivityModule;
import com.pasotti.matteo.apptictrac.dagger.module.MainActModule;
import com.pasotti.matteo.apptictrac.pojo.User;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View, UsersAdapter.onUserSelected, TextWatcher {

    @BindView(R.id.rv_users)
    RecyclerView rv;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title_toolbar)
    TextView title_toolbar;

    @BindView(R.id.search_edittext)
    EditText search_toolbar;

    @Inject
    MainPresenter mPresenter;

    @Inject
    UsersAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    private List<User> users;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerMainActComponent.builder()
                .netComponent(((MyApplication) getApplicationContext()).getNetComponent())
                .activityModule(new ActivityModule(this))
                .mainActModule(new MainActModule(this))
                .build().inject(this);

        initView();

        mPresenter.getUsers();

    }

    private void initView() {
        setSupportActionBar(toolbar);
        title_toolbar.setText(getResources().getString(R.string.app_name).toString());
        search_toolbar.addTextChangedListener(this);

        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);
        adapter.setOnUserSelected(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_save) {
            mPresenter.saveUsersOnPref(adapter.getUsers());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initListUsers(List<User> items) {

        users = items;
        adapter.initUserList(items);
    }

    @Override
    public void filterList(String filter) {
        List<User> filteredUsers = new ArrayList<>();

        if(!filter.equals("")) {
            if (users != null && users.size() > 0) {
                for (User u : users) {
                    if (u.getName().toLowerCase().startsWith(filter) || u.getEmail().toLowerCase().startsWith(filter)) {
                        filteredUsers.add(u);
                    }
                }
            }
            adapter.updateUsers(filteredUsers);
        }else {
            mPresenter.getUsers();
        }



    }

    @Override
    public void onSelected(User userSelected) {
        title_toolbar.setText(userSelected.getName());
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        filterList(editable.toString());
    }
}
