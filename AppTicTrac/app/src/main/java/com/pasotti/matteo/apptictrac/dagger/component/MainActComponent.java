package com.pasotti.matteo.apptictrac.dagger.component;


import com.pasotti.matteo.apptictrac.dagger.module.ActivityModule;
import com.pasotti.matteo.apptictrac.dagger.module.MainActModule;
import com.pasotti.matteo.apptictrac.mainscreen.MainActivity;
import com.pasotti.matteo.apptictrac.util.ActivityScope;

import dagger.Component;

@Component(dependencies = NetComponent.class, modules = {MainActModule.class, ActivityModule.class})
@ActivityScope
public interface MainActComponent {

    void inject(MainActivity activity);
}
