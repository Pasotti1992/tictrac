package com.pasotti.matteo.apptictrac.api;


import com.pasotti.matteo.apptictrac.pojo.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserApiInterface {

    @GET("/tmp/users.json")
    Call<List<User>> getUsers();
}
