package com.pasotti.matteo.apptictrac.dagger.module;

import android.app.Application;

import com.pasotti.matteo.apptictrac.util.LocalStorage;
import com.pasotti.matteo.apptictrac.util.SharedPrefStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Application application;

    public AppModule(Application mApplication) {
        this.application = mApplication;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    SharedPrefStorage provideLocalStorage() { return new SharedPrefStorage(application); }

}
