package com.pasotti.matteo.apptictrac.common;

import android.app.Application;

import com.pasotti.matteo.apptictrac.dagger.component.DaggerNetComponent;
import com.pasotti.matteo.apptictrac.dagger.component.NetComponent;
import com.pasotti.matteo.apptictrac.dagger.module.AppModule;
import com.pasotti.matteo.apptictrac.dagger.module.NetModule;


public class MyApplication extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
}
