package com.pasotti.matteo.apptictrac.mainscreen;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pasotti.matteo.apptictrac.R;
import com.pasotti.matteo.apptictrac.pojo.User;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    private List<User> users;

    private User userSelected = null;

    private onUserSelected onUserSelected;

    private Context context;

    @Inject
    public UsersAdapter(Context context) {
        this.context = context;
        this.users = new ArrayList<>();
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_row, parent, false);

        final UserViewHolder holder = new UserViewHolder(view);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onUserSelected != null) {
                    userSelected = users.get(holder.getAdapterPosition());
                    onUserSelected.onSelected(userSelected);
                    notifyDataSetChanged();
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {

        User user = users.get(position);
        if(userSelected != null && userSelected.equals(user)) {
            holder.name.setTextColor(context.getResources().getColor(R.color.blue));
            holder.name.setTypeface(Typeface.DEFAULT_BOLD);
            holder.infos.setText(user.getInfos());
            holder.infos.setVisibility(View.VISIBLE);
        } else {
            holder.name.setTextColor(context.getResources().getColor(R.color.black));
            holder.name.setTypeface(Typeface.DEFAULT);
            holder.infos.setVisibility(View.GONE);
        }

        holder.name.setText(user.getName());
        holder.email.setText(user.getEmail());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void initUserList(List<User> items) {
        this.users.clear();
        this.users.addAll(items);
        notifyDataSetChanged();
    }

    public void updateUsers(List<User> items) {
        this.users.clear();
        this.users.addAll(items);
        notifyDataSetChanged();
    }

    public List<User> getUsers() {return users; }

    public interface onUserSelected {

        void onSelected(User userSelected);
    }

    public void setOnUserSelected(UsersAdapter.onUserSelected onUserSelected) {
        this.onUserSelected = onUserSelected;
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_name)
        TextView name;

        @BindView(R.id.user_email)
        TextView email;

        @BindView(R.id.user_infos)
        TextView infos;

        @BindView(R.id.button_set)
        Button button;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
