package com.pasotti.matteo.apptictrac.mainscreen;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.pasotti.matteo.apptictrac.api.UserApiInterface;
import com.pasotti.matteo.apptictrac.pojo.User;
import com.pasotti.matteo.apptictrac.util.LocalStorage;
import com.pasotti.matteo.apptictrac.util.SharedPrefStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainPresenter implements MainContract.Presenter {

    private String TAG = "MainPresenter";

    Retrofit retrofit;

    MainContract.View mView;

    SharedPrefStorage localStorage;

    Activity activity;

    @Inject
    public MainPresenter(Retrofit retrofit, Activity activity,SharedPrefStorage localStorage, MainContract.View view ) {
        this.retrofit = retrofit;
        this.activity = activity;
        this.localStorage = localStorage;
        this.mView = view;
    }


    @Override
    public void getUsers() {

        if(isNetworkAvailable()) {
            retrofit.create(UserApiInterface.class).getUsers().enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    Log.d(TAG, "getUsers() onResponse");
                    if(response.body() != null && response.body().size() > 0) {
                        mView.initListUsers(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Log.d(TAG, "getUsers() onFailure");
                    //the link doesn't work
                    mView.initListUsers(extractUsersFromFile("users.json"));
                }
            });
        } else {
            mView.showMessage("get Users from Prefs!");
            mView.initListUsers(localStorage.getUsersFromPref());
        }

    }

    @Override
    public void saveUsersOnPref(List<User> users) {

        if(users != null && users.size() > 0) {
            for(User user: users) {
                localStorage.addUserToPreferences(user);
            }

            mView.showMessage("Users Saved! Close the app, turn off your internet connection and check!");

        }

    }

    @Override
    public List<User> extractUsersFromFile(String filename) {

        List<User> users = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJsonFromAssets(filename));
            JSONArray jArray = obj.getJSONArray("users");
            HashMap<String, String> m_li;

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jobject = jArray.getJSONObject(i);
                String name = jobject.getString("name");
                String email = jobject.getString("email");
                String infos = jobject.getString("infos");
                users.add(new User(name, email, infos));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  users;
    }

    @Override
    public String loadJsonFromAssets(String filename) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
