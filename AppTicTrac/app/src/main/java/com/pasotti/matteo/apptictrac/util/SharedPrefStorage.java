package com.pasotti.matteo.apptictrac.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pasotti.matteo.apptictrac.pojo.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefStorage implements LocalStorage {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SharedPrefStorage(Context context) {
        this.preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        this.editor = preferences.edit();
    }

    @Override
    public void addUserToPreferences(User user) {
        Gson gson = new Gson();
        List<User> usersPref = getUsersFromPref();

        if(usersPref != null && usersPref.size() > 0) {
            usersPref.add(user);
        } else {
            usersPref = new ArrayList<>();
            usersPref.add(user);
        }

        String json = gson.toJson(usersPref);
        editor.putString("usersPref", json);
        editor.commit();
    }

    @Override
    public void removeUserFromPreferences(User user) {

        Gson gson = new Gson();
        List<User> usersPref = getUsersFromPref();

        if(usersPref != null && usersPref.size() > 0) {
            usersPref.remove(user);
        }

        String json = gson.toJson(usersPref);
        editor.putString("usersPref", json);
        editor.commit();

    }

    @Override
    public List<User> getUsersFromPref() {
        Gson gson = new Gson();
        String json = preferences.getString("usersPref", null);
        Type type = new TypeToken<ArrayList<User>>() {
        }.getType();
        ArrayList<User> arrayList = gson.fromJson(json, type);

        return arrayList;
    }
}
