package com.pasotti.matteo.apptictrac.mainscreen;


import com.pasotti.matteo.apptictrac.pojo.User;

import java.util.List;

public interface MainContract {

    interface View {

        void showMessage(String message);

        void initListUsers(List<User> users);

        void filterList(String filter);
    }

    interface Presenter {

        void getUsers();

        void saveUsersOnPref(List<User> users);

        List<User> extractUsersFromFile(String filename);

        String loadJsonFromAssets(String filename);

        boolean isNetworkAvailable();
    }
}
